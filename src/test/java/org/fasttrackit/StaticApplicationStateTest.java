package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;
import org.fasttrackit.pages.MainPage;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.open;
import static org.testng.Assert.*;

public class StaticApplicationStateTest {

    MainPage homepage = new MainPage();

    @Test
    public void verify_demo_shop_app_title() {
        String appTitle = homepage.getPageTitle();
        assertEquals(appTitle, "Demo shop", "Application title is expected to be Demo shop.");
    }

    @Test
    public void verify_demo_shop_footer_build_date_details() {
        String footerDetails = homepage.getFooter().getDetails();
        assertEquals(footerDetails, "Demo Shop | build date 2021-05-21 14:04:30 GTBDT", "Expected footer details to be: Demo Shop | build date 2021-05-21 14:04:30 GTBDT");
    }

    @Test
    public void verify_demo_shop_footer_contains_questionIcon() {
        SelenideElement questionIcon = homepage.getFooter().getQuestionIcon();
        assertTrue(questionIcon.exists(), "Expected Question Icon to exist on Page.");
        assertTrue(questionIcon.isDisplayed(), "Expected Question icon to be displayed.");
    }

    @Test
    public void verify_demo_shop_footer_contains_resetIcon() {
        SelenideElement resetIconTitle = homepage.getFooter().getResetIcon();
        assertTrue(resetIconTitle.exists(), "Expected Reset icon to exist on Page.");
        assertTrue(resetIconTitle.isDisplayed(), "Expected Reset icon to be displayed.");
    }

    @Test
    public void verify_demo_shop_header_contains_wishlistIcon() {
        SelenideElement wishlistIconUrl = homepage.getHeader().getWishlistIconUrl();
        assertTrue(wishlistIconUrl.exists(), "Expected Wishlist icon to exist on Page.");
        assertTrue(wishlistIconUrl.isDisplayed(), "Expected Wishlist icon to be displayed.");
    }

    @Test
    public void verify_demo_shop_header_contains_shoppingCartIcon() {
        SelenideElement shoppingCartIconUrl = homepage.getHeader().getShoppingCartIconUrl();
        assertTrue(shoppingCartIconUrl.exists(), "Expected Shopping cart icon to exist on Page.");
        assertTrue(shoppingCartIconUrl.isDisplayed(), "Expected Shopping cart icon to be displayed.");
    }
}
