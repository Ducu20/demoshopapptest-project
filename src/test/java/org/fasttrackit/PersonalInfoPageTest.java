package org.fasttrackit;

import org.fasttrackit.dataprovider.ProductDataProvider;
import org.fasttrackit.pages.CartPage;
import org.fasttrackit.pages.MainPage;
import org.fasttrackit.pages.PersonalInfoPage;
import org.fasttrackit.products.Product;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.assertTrue;

public class PersonalInfoPageTest {

    MainPage homePage = new MainPage();


    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "productsDataProvider")
    public void user_can_type_personal_info(Product product) {
        product.addProductToBasket();
        homePage.goToCart();
        CartPage cartPage = new CartPage(product);
        cartPage.clickOnCheckoutButton();
        PersonalInfoPage personalInfoPage = new PersonalInfoPage();
        Person person = new Person("Mihai", "Popescu", "Strada Mare, nr.10, Cluj-Napoca, jud.Cluj");
        personalInfoPage.typeInFirstNameField(person.getFirstName());
        assertTrue(personalInfoPage.validateFirstNameFieldIsDisplayed(), "Expected first name field to be displayed.");
        personalInfoPage.typeInLastNameField(person.getLastName());
        assertTrue(personalInfoPage.validateLastNameFieldIsDisplayed(), "Expected last name field to be displayed.");
        personalInfoPage.typeInAddressField(person.getAddress());
        assertTrue(personalInfoPage.validateAddressFieldIsDisplayed(), "Expected address field to be displayed.");
        sleep(3000);
        assertTrue(personalInfoPage.validatePersonalInfoDetailsAreDisplayed(), "Expected personal info details to be displayed.");
        homePage.returnToHomePage();

    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "productsDataProvider")
    public void user_can_select_credit_card_payment_method(Product product) {
        product.addProductToBasket();
        homePage.goToCart();
        CartPage cartPage = new CartPage(product);
        cartPage.clickOnCheckoutButton();
        PersonalInfoPage personalInfoPage = new PersonalInfoPage();
        personalInfoPage.selectCreditCardPaymentMethod();
        sleep(3000);
        assertTrue(personalInfoPage.validateCreditCardOptionIsSelectedAndDisplayed(), "Expected to credit card option to be selected.");
    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "productsDataProvider")
    public void user_can_click_on_continue_checkout_button(Product product) {
        product.addProductToBasket();
        homePage.goToCart();
        CartPage cartPage = new CartPage(product);
        cartPage.clickOnCheckoutButton();
        PersonalInfoPage personalInfoPage = new PersonalInfoPage();
        Person person = new Person("Mihai", "Popescu", "Strada Mare, nr.10, Cluj-Napoca, jud.Cluj");
        personalInfoPage.typeInFirstNameField(person.getFirstName());
        personalInfoPage.typeInLastNameField(person.getLastName());
        personalInfoPage.typeInAddressField(person.getAddress());
        personalInfoPage.clickOnContinueCheckoutButton();
        sleep(3000);
        assertTrue(personalInfoPage.validateOrderSummaryPageIsDisplayed(), "Expected order summary page to be displayed.");
        homePage.returnToHomePage();
    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "productsDataProvider")
    public void user_can_click_on_cancel_button(Product product) {
        product.addProductToBasket();
        homePage.goToCart();
        CartPage cartPage = new CartPage(product);
        cartPage.clickOnCheckoutButton();
        PersonalInfoPage personalInfoPage = new PersonalInfoPage();
        personalInfoPage.clickOnCancelButton();
        sleep(2000);
        homePage.returnToHomePage();
        assertTrue(cartPage.validateCartPageIsDisplayed(), "Expected cart page to be displayed.");
    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "productsDataProvider")
    public void first_name_error_message_is_displayed(Product product) {
        product.addProductToBasket();
        homePage.goToCart();
        CartPage cartPage = new CartPage(product);
        cartPage.clickOnCheckoutButton();
        PersonalInfoPage personalInfoPage =new PersonalInfoPage();
        personalInfoPage.clickOnContinueCheckoutButton();
        sleep(2000);
        assertTrue(personalInfoPage.validateFirstNameErrorMessageIsDisplayed(), "Expected first name error message to be displayed.");
        homePage.returnToHomePage();
    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "productsDataProvider")
    public void last_name_error_message_is_displayed(Product product) {
        product.addProductToBasket();
        homePage.goToCart();
        CartPage cartPage = new CartPage(product);
        cartPage.clickOnCheckoutButton();
        PersonalInfoPage personalInfoPage = new PersonalInfoPage();
        Person person = new Person("Mihai", "", "");
        personalInfoPage.typeInFirstNameField(person.getFirstName());
        personalInfoPage.clickOnContinueCheckoutButton();
        sleep(2000);
        assertTrue(personalInfoPage.validateLastNameErrorMessageIsDisplayed(), "Expected last name error message to be displayed.");
        homePage.returnToHomePage();
    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "productsDataProvider")
    public void address_error_message_is_displayed(Product product) {
        product.addProductToBasket();
        homePage.goToCart();
        CartPage cartPage = new CartPage(product);
        cartPage.clickOnCheckoutButton();
        PersonalInfoPage personalInfoPage = new PersonalInfoPage();
        Person person = new Person("Mihai", "Popescu", "");
        personalInfoPage.typeInFirstNameField(person.getFirstName());
        personalInfoPage.typeInLastNameField(person.getLastName());
        personalInfoPage.clickOnContinueCheckoutButton();
        sleep(2000);
        assertTrue(personalInfoPage.validateAddressErrorMessageIsDisplayed(), "Expected address error message to be displayed.");
        homePage.returnToHomePage();
    }

}
