package org.fasttrackit;

import org.fasttrackit.pages.MainPage;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.assertTrue;

public class SortDropDownMenuTest {
    MainPage homepage = new MainPage();

    @Test
    public void user_can_click_on_sort_menu_components() {
        homepage.clickOnTheSortField();
        sleep(3000);
        assertTrue(homepage.validateSortDropDownMenuIsDisplayed(), "Expected that sort drop down menu to be displayed.");
        homepage.clickOnSortZtoA();
        sleep(3000);
        assertTrue(homepage.validateSortZtoAIsDisplayed(), "Expected that Z to A sort to be displayed.");
        homepage.clickOnSortLowToHigh();
        sleep(3000);
        assertTrue(homepage.validateSortLowToHighIsDisplayed(), "Expected that Low to High sort to be displayed.");
        homepage.clickOnSortHighToLow();
        sleep(3000);
        assertTrue(homepage.validateSortHighToLowIsDisplayed(), "Expected that High to Low sort to be displayed.");
        homepage.clickOnSortAToZ();
        sleep(3000);
        assertTrue(homepage.validateSortAToZIsDisplayed(), "Expected that A to Z sort to be displayed.");
    }
}
