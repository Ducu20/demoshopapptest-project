package org.fasttrackit;

import org.fasttrackit.dataprovider.ProductDataProvider;
import org.fasttrackit.pages.CartPage;
import org.fasttrackit.pages.MainPage;
import org.fasttrackit.pages.ProductDetailsPage;
import org.fasttrackit.products.Product;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.*;

public class ProductTest {

    MainPage homePage = new MainPage();

    @AfterMethod
    public void returnToHomePage() {
        MainPage homePage = new MainPage();
        homePage.clickOnTheLogoIcon();
    }

    @Test(testName = "Click on product test", dataProvider = "productsDataProvider", dataProviderClass = ProductDataProvider.class)
    public void user_can_click_on_a_product(Product product) {
        product.clickOnProduct();
        sleep(2000);
        ProductDetailsPage productPage = new ProductDetailsPage();
        String expectedTitle = product.getExpectedResults().getTitle();
        assertEquals(productPage.getTitle(), expectedTitle, "Expected title to be " + expectedTitle);
        sleep(2000);
        homePage.clickOnTheResetIcon();
    }

    @Test(testName = "Add product(s) to basket test", dataProvider = "productsDataProvider", dataProviderClass = ProductDataProvider.class)
    public void user_can_add_product_to_basket_test(Product product) {
        product.addProductToBasket();
        sleep(3000);
        assertTrue(homePage.getProductAddedToBasket(), "Expected product(s) to be added to basket.");
        homePage.clickOnTheResetIcon();
    }

    @Test(testName = "Add product(s) to wishlist test", dataProvider = "productsDataProvider", dataProviderClass = ProductDataProvider.class)
    public void user_can_add_product_to_wishlist_test(Product product) {
        product.addToFavorites();
        sleep(3000);
        assertTrue(homePage.getProductAddedToWishlist(), "Expected product(s) to be added to wishlist.");
        homePage.clickOnTheResetIcon();
    }


}
