package org.fasttrackit.pages;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.fasttrackit.body.Footer;
import org.fasttrackit.body.Header;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;


public class MainPage extends Page {
    final String title = Selenide.title();
    private Header header;
    private final Footer footer;

    public SelenideElement productAddedToBasket = $(".navbar-text [href='#/cart']");
    public SelenideElement productAddedToWishlist = $(".navbar-text [href='#/wishlist']");
    private final SelenideElement cart = $(".fa-shopping-cart");
    private final SelenideElement modal = $(".modal-dialog");
    private final SelenideElement questionIconModal = $(".modal-content");
    private final SelenideElement sortField = $(".sort-products-select");
    private final SelenideElement sortAtoZ = $("[value='az']");
    private final SelenideElement sortZtoA = $("[value='za']");
    private final SelenideElement sortLowToHigh = $("[value='lohi']");
    private final SelenideElement sortHighToLow = $("[value='hilo']");
    private final SelenideElement searchField = $("#input-search");
    private final SelenideElement searchButton = $(".btn-light");
    private final SelenideElement mainPage = $("#root");



    public MainPage() {
        System.out.println("Constructing Header");
        this.header = new Header();
        System.out.println("Constructing Footer");
        this.footer = new Footer();
    }

    public void returnToHomePage() {
        this.header.getLogoIconUrl().click();
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public Footer getFooter() {
        return footer;
    }

    public Header getHeader() {
        return header;
    }


    public void validateThatFooterContainsAllElements() {
        System.out.println("1. Verify footer details are: " + footer.getDetails());
        System.out.println("2. Verify footer has Question Icon: " + footer.getQuestionIcon());
        System.out.println("3. Verify footer reset Icon title: " + footer.getResetIcon());
    }

    public void validateThatHeaderContainsAllElements() {
        System.out.println("1. Verify that logo url is : " + header.getLogoIconUrl());
        System.out.println("2. Verify that shopping cart url is: " + header.getShoppingCartIconUrl());
        System.out.println("3. Verify that wishlist url is: " + header.getWishlistIconUrl());
        System.out.println("4. Verify that welcome message is: " + header.getGreetingsMessage());
        System.out.println("5. Verify that sign-in is: " + header.getSignInButton());
    }

    public void clickOnTheLoginButton() {
        this.header.clickOnTheLoginButton();

    }

    public void validateWishlistIcon() {
        System.out.println("-------");
        this.header.clickOnTheWishlistIcon();
    }

    public void logUserOut() {
        this.header.clickOnTheLogoutButton();
    }


    public void goToCart() {
        this.cart.click();
    }
    public void goToWishlist() {
        this.productAddedToWishlist.click();
    }


    /**
     * Clicks
     */

    public void clickOnTheSortField() {
        this.sortField.click();
    }

    public void clickOnSortZtoA() {
        this.sortZtoA.click();
    }

    public void clickOnSortLowToHigh() {
        this.sortLowToHigh.click();
    }
    public void clickOnSortHighToLow() {
        this.sortHighToLow.click();
    }
    public void clickOnSortAToZ() {
        this.sortAtoZ.click();
    }

    public void clickOnTheSearchField() {
        this.searchField.click();
    }

    public void clickOnSearchButton() {
        this.searchButton.click();
    }
    public void clickOnTheResetIcon() {
        this.footer.clickOnTheResetIcon();
    }
    public void clickOnTheQuestionIcon() {
        this.footer.clickOnTheQuestionIcon();
    }
    public void clickOnTheLogoIcon() {
        System.out.println("-------");
        this.header.clickOnTheLogoIcon();
    }


    /**
     * Validators
     */

    public void validateQuestionIconModalIsNotDisplayed() {

    }

    public boolean validateItemAddedIsNotDisplayed() {
        return this.productAddedToBasket.isDisplayed();

    }

    public boolean validateSortDropDownMenuIsDisplayed() {
        return this.sortField.exists() && this.sortField.isDisplayed();
    }

    public boolean validateSortZtoAIsDisplayed() {
        return this.sortZtoA.exists() && this.sortZtoA.isDisplayed();
    }
    public boolean validateSortLowToHighIsDisplayed() {
        return this.sortLowToHigh.exists() && this.sortLowToHigh.isDisplayed();
    }
    public boolean validateSortHighToLowIsDisplayed() {
        return this.sortHighToLow.exists() && this.sortHighToLow.isDisplayed();
    }

    public boolean validateSortAToZIsDisplayed() {
        return this.sortAtoZ.exists() && this.sortAtoZ.isDisplayed();
    }

    public boolean validateModalIsNotDisplayed() {
        sleep(1000);
        boolean exists = !modal.exists();
        System.out.println("1. Verify that modal is not on page. exists" + exists);
        return exists;

    }

    public boolean validateQuestionIconModalIsDisplayed() {
        return this.questionIconModal.exists() && this.questionIconModal.isDisplayed();
    }

    public boolean validateModalIsDisplayed() {
        System.out.println("Verify that the modal is displayed on page.");
        return this.modal.exists() && this.modal.isDisplayed();
    }
    public boolean validateMainPageIsDisplayed() {
        return this.mainPage.isDisplayed();
    }
    public boolean validateSearchFieldIsDisplayed() {
        return this.searchField.exists() && this.searchField.isDisplayed();
    }

    public void validateCartIcon() {
        System.out.println("-------");
        this.header.clickOnTheCartIcon();
    }


    /**
     * Getters
     */

    public boolean getProductAddedToBasket() {
        return this.productAddedToBasket.isDisplayed();
    }

    public boolean getProductAddedToWishlist() {
        return this.productAddedToWishlist.isDisplayed();
    }
    public String getPageTitle() {
        return title;
    }

    /**
     * Types
     */

    public void typeInSearchField(String productLink) {
        System.out.println("Typed in search field: " + productLink);
        this.searchField.sendKeys(productLink);
    }



}
