package org.fasttrackit.pages;

import com.codeborne.selenide.SelenideElement;
import org.fasttrackit.products.Product;

import static com.codeborne.selenide.Selenide.$;

public class WishlistPage {
    private final String pageSubtitle;
    private final SelenideElement productInWishlist;
    private final SelenideElement logoIcon;
    private final SelenideElement wishlistPage;
    private final SelenideElement brokenHeartButton;
    private final SelenideElement cartButton;
    private final SelenideElement headerCartButton;

    public WishlistPage(Product product) {
        this.pageSubtitle = "Wishlist";
        System.out.println("Wishlist page was created!");
        this.productInWishlist = $(".card-link[href='#/product/%s']");
        this.logoIcon = $(".fa-shopping-bag");
        this.wishlistPage = $(".container-fluid");
        this.brokenHeartButton = $(".fa-heart-broken");
        this.cartButton = $(".fa-cart-plus");
        this.headerCartButton = wishlistPage.$(".navbar-text [href='#/cart']");

    }



    public void validateThatTheWishlistPageIsDisplayed() {
        System.out.println("Verify that the Wishlist page is displayed.");
    }

    public void clickOnTheLogoIcon() {
        this.logoIcon.click();
    }
    public void clickOnBrokenHeartButton() {
        this.brokenHeartButton.click();
    }
    public void clickOnCartButton() {
        this.cartButton.click();
    }

    public boolean validateWishlistPageIsDisplayed() {
        return this.wishlistPage.exists() && this.wishlistPage.isDisplayed();
    }
    public boolean validateProductIsNotDisplayed() {
        return !this.productInWishlist.exists();
    }
    public boolean validateProductIsDisplayedInCart() {
        return this.headerCartButton.isDisplayed();
    }


}

