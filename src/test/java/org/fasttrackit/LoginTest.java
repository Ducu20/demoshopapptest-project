package org.fasttrackit;

import org.fasttrackit.body.Header;
import org.fasttrackit.body.Modal;
import org.fasttrackit.dataprovider.AccountDataProvider;
import org.fasttrackit.pages.MainPage;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;


import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.*;

public class LoginTest {

    MainPage homepage;

    @BeforeTest
    public void setup() {
        homepage = new MainPage();
        homepage.returnToHomePage();
    }

    @AfterMethod
    public void tearDown() {
        System.out.println("I will clean up after each test. !!");
    }

    @AfterTest
    public void tearDownClass() {
        System.out.println("I will clean up after test class. !!");
    }


    @Test

    public void modal_components_are_displayed_and_modal_can_be_closed_test() {
        homepage.clickOnTheLoginButton();
        assertTrue(homepage.validateModalIsDisplayed(), "Expected modal is displayed.");
        Modal modal = new Modal();
        assertEquals(modal.getModalTitle(), "Login", "Expected Modal title to be Login.");
        assertTrue(modal.validateCloseButtonIsDisplayed(), "Expected Close Button to be displayed.");
        assertTrue(modal.validateUserNameFieldIsDisplayed(), "Expected username field to be displayed.");
        assertTrue(modal.validatePasswordFieldIsDisplayed(), "Expected password field to be displayed.");
        assertTrue(modal.validateLoginButtonIsDisplayedAndEnable(), "Expected login button to be displayed and enable.");
        modal.validateModalComponents();
        modal.clickOnCloseButton();
        assertTrue(homepage.validateModalIsNotDisplayed(), "Expected modal to be closed.");


    }

    @Test(testName = "Login Test with valid User:",
            dataProvider = "AccountDataProvider",
            dataProviderClass = AccountDataProvider.class)
    public void user_can_login_on_the_demo_app(Account account) {
        homepage.clickOnTheLoginButton();
        Modal modal = new Modal();
        modal.typeInUsernameField(account.getUsername());
        modal.typeInPasswordField(account.getPassword());
        modal.clickOnTheLoginButton();
        assertTrue(homepage.validateModalIsNotDisplayed(), "Expected modal to be closed.");
        Header header = new Header(account.getUsername());
        assertEquals(header.getGreetingsMessage(), account.getGreetingsMsg(), "Expected greetings message to contain the account user.");
        homepage.logUserOut();




    }

    @Test(testName = "Login Test",
            description = "This test is responsible for testing that the user can't login on page without password",
            dependsOnMethods = {"modal_components_are_displayed_and_modal_can_be_closed_test", "user_can_login_on_the_demo_app"})

    public void user_cant_login_on_page_without_password_test() {
        homepage.clickOnTheLoginButton();
        Modal modal = new Modal();
        String user = "beetle";
        modal.typeInUsernameField(user);
        modal.clickOnTheLoginButton();
        assertTrue(homepage.validateModalIsDisplayed(), "Expected modal to remain on page.");
        modal.clickOnCloseButton();

        sleep(3000);


    }

    @Test
    public void modal_attention_message_is_displayed_test() {
        homepage.clickOnTheLoginButton();
        Modal modal = new Modal();
        modal.clickOnTheLoginButton();
        assertTrue(modal.validateAttentionMessageIsDisplayed(), "Expected attention message to be displayed.");
        modal.clickOnCloseButton();

    }

    @Test
    public void user_cannot_login_with_locked_account() {
        Account lockedAccount = new Account("locked", "choochoo");
        homepage.clickOnTheLoginButton();
        Modal modal = new Modal();
        modal.typeInUsernameField(lockedAccount.getUsername());
        modal.typeInPasswordField(lockedAccount.getPassword());
        modal.clickOnTheLoginButton();
        assertTrue(homepage.validateModalIsDisplayed(), "Expected modal to remain open.");
        // assert that error message appears.
        modal.clickOnCloseButton();

    }
}
