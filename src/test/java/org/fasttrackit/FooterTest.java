package org.fasttrackit;

import org.fasttrackit.body.QuestionIconModal;
import org.fasttrackit.pages.MainPage;
import org.fasttrackit.products.Product;
import org.fasttrackit.products.ProductExpectedResults;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class FooterTest {

    MainPage homePage = new MainPage();

    @Test

    public void user_can_click_on_question_icon_and_question_icon_modal_is_displayed_test() {
        homePage.clickOnTheQuestionIcon();
        assertTrue(homePage.validateQuestionIconModalIsDisplayed(), "Expected question icon modal is displayed.");
        QuestionIconModal questionIconModal = new QuestionIconModal();
        sleep(5000);
        questionIconModal.clickOnCloseButton();


    }

    @Test
    public void user_can_click_on_reset_icon_test() {
        Product product = new Product("1", new ProductExpectedResults("", ""));
        sleep(3000);
        homePage.clickOnTheResetIcon();
        sleep(3000);
        assertTrue(homePage.validateItemAddedIsNotDisplayed(), "Expected product(s) added to cart/wishlist to not exist.");
    }
}
