package UserTests;

import org.fasttrackit.Account;
import org.fasttrackit.Person;
import org.fasttrackit.body.Modal;
import org.fasttrackit.dataprovider.ProductDataProvider;
import org.fasttrackit.pages.*;
import org.fasttrackit.products.Product;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.assertTrue;

public class DinoUserTest {
    MainPage homePage = new MainPage();

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "productsDataProvider")
    public void dino_user_can_complete_a_order(Product product) {
        Account account = new Account("dino", "choochoo");
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        modal.typeInUsernameField(account.getUsername());
        modal.typeInPasswordField(account.getPassword());
        modal.clickOnTheLoginButton();
        product.addProductToBasket();
        homePage.goToCart();
        CartPage cartPage = new CartPage(product);
        cartPage.clickOnCheckoutButton();
        PersonalInfoPage personalInfoPage = new PersonalInfoPage();
        Person person = new Person("Mihai", "Popescu", "Strada Mare, nr.10, Cluj-Napoca, jud.Cluj");
        personalInfoPage.typeInFirstNameField(person.getFirstName());
        personalInfoPage.typeInLastNameField(person.getLastName());
        personalInfoPage.typeInAddressField(person.getAddress());
        personalInfoPage.clickOnContinueCheckoutButton();
        OrderSummaryPage orderSummaryPage = new OrderSummaryPage();
        orderSummaryPage.clickOnCompleteYourOrderButton();
        OrderCompletePage orderCompletePage = new OrderCompletePage();
        assertTrue(orderCompletePage.validateOrderCompleteMessageIsDisplayed(), "Expected order complete message to be displayed.");
        sleep(2000);
        homePage.returnToHomePage();
        homePage.clickOnTheResetIcon();
    }
}
