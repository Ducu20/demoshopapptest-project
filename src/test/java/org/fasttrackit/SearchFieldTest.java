package org.fasttrackit;

import org.fasttrackit.dataprovider.ProductDataProvider;
import org.fasttrackit.pages.MainPage;
import org.fasttrackit.products.Product;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.assertTrue;

public class SearchFieldTest {

    MainPage homePage = new MainPage();

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "productsDataProvider")
    public void user_can_type_products_in_search_field(Product product) {
        homePage.clickOnTheSearchField();
        sleep(3000);
        homePage.typeInSearchField(product.getProductLink());
        sleep(3000);
        homePage.clickOnSearchButton();
        sleep(3000);
        homePage.clickOnTheResetIcon();
        sleep(3000);

    }
    @Test
    public void verify_search_field_is_displayed() {
        assertTrue(homePage.validateSearchFieldIsDisplayed(), "Expected that search field to be displayed.");
    }

}
