package org.fasttrackit;

import org.fasttrackit.dataprovider.ProductDataProvider;
import org.fasttrackit.pages.*;
import org.fasttrackit.products.Product;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.assertTrue;

public class OrderSummaryPageTest {

    MainPage homepage = new MainPage();

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "productsDataProvider")
    public void user_can_click_on_complete_your_order_button(Product product) {
        product.addProductToBasket();
        homepage.goToCart();
        CartPage cartPage = new CartPage(product);
        cartPage.clickOnCheckoutButton();
        PersonalInfoPage personalInfoPage = new PersonalInfoPage();
        Person person = new Person("Mihai", "Popescu", "Strada Mare, nr.10, Cluj-Napoca, jud.Cluj");
        personalInfoPage.typeInFirstNameField(person.getFirstName());
        personalInfoPage.typeInLastNameField(person.getLastName());
        personalInfoPage.typeInAddressField(person.getAddress());
        personalInfoPage.clickOnContinueCheckoutButton();
        OrderSummaryPage orderSummaryPage = new OrderSummaryPage();
        orderSummaryPage.clickOnCompleteYourOrderButton();
        sleep(2000);
        OrderCompletePage orderCompletePage = new OrderCompletePage();
        assertTrue(orderCompletePage.validateOrderCompleteMessageIsDisplayed(), "Expected that the order complete message('Thank you for your order') to be displayed.");
        homepage.returnToHomePage();

    }

    @Test(dataProviderClass =  ProductDataProvider.class, dataProvider = "productsDataProvider")
    public void user_can_click_on_cancel_button(Product product) {
        product.addProductToBasket();
        homepage.goToCart();
        CartPage cartPage = new CartPage(product);
        cartPage.clickOnCheckoutButton();
        PersonalInfoPage personalInfoPage = new PersonalInfoPage();
        Person person = new Person("Mihai", "Popescu", "Strada Mare, nr.10, Cluj-Napoca, jud.Cluj");
        personalInfoPage.typeInFirstNameField(person.getFirstName());
        personalInfoPage.typeInLastNameField(person.getLastName());
        personalInfoPage.typeInAddressField(person.getAddress());
        personalInfoPage.clickOnContinueCheckoutButton();
        OrderSummaryPage orderSummaryPage = new OrderSummaryPage();
        orderSummaryPage.clickOnCancelButton();
        sleep(2000);
        assertTrue(cartPage.validateCartPageIsDisplayed(), "Expected cart page to be displayed.");
        homepage.returnToHomePage();
    }
}
