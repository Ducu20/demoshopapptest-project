package org.fasttrackit.pages;

import com.codeborne.selenide.SelenideElement;
import org.fasttrackit.products.Product;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class CartPage {

    private final String pageSubtitle;
    private final SelenideElement productLinKCart;
    private final SelenideElement shoppingCart;
    private final SelenideElement trashIcon;
    private final SelenideElement plusButton;
    private final SelenideElement minusButton;
    private final SelenideElement continueShoppingButton;
    private final SelenideElement checkoutButton;
    private final SelenideElement infoPage;
    private final SelenideElement cartPage;


    public CartPage(Product product) {
        this.pageSubtitle = "Your cart";
        System.out.println("Cart page was created.");
        this.productLinKCart = $(String.format("#item_%s_title_link", product.getProductId()));
        this.shoppingCart = productLinKCart.parent().parent();
        this.trashIcon = shoppingCart.$(".fa-trash");
        this.plusButton = shoppingCart.$(".fa-plus-circle");
        this.minusButton = shoppingCart.$(".fa-minus-circle");
        this.continueShoppingButton = $(".btn-danger[href='#/products']");
        this.checkoutButton = $(".btn-success");
        this.infoPage = $(".container-fluid");
        this.cartPage = $("#root");
        System.out.println(productLinKCart.getText());
    }

    public void validateThatTheCartPageIsDisplayed() {
        System.out.println("Verify that the Cart page is displayed.");
    }

    public void clickOnTrashIcon() {
        this.trashIcon.click();
    }

    public void clickOnPlusButton() {
        this.plusButton.click();
    }

    public void clickOnMinusButton() {
        this.minusButton.click();
    }

    public void clickOnContinueShoppingButton() {
        this.continueShoppingButton.click();
    }
    public void clickOnCheckoutButton() {
        this.checkoutButton.click();
    }

    public boolean validateProductIsDeleteFromCart() {
        System.out.println("Verify that the product exist in cart.");
        return !this.productLinKCart.exists();
    }

    public boolean validateProductIsIncreaseInCart() {
        System.out.println("Verify that the product is increase.");
        return this.productLinKCart.exists();
    }

    public boolean validateProductIsDecreaseInCart() {
        System.out.println("Verify that the product is decrease.");
        return this.productLinKCart.exists();
    }

    public boolean validateCartPageIsDisplayed() {
        return this.cartPage.isDisplayed();
    }


}
