package UserTests;

import org.fasttrackit.Account;
import org.fasttrackit.Person;
import org.fasttrackit.body.Header;
import org.fasttrackit.body.Modal;
import org.fasttrackit.dataprovider.ProductDataProvider;
import org.fasttrackit.pages.*;
import org.fasttrackit.products.Product;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class BeetleUserTest {


    MainPage homePage = new MainPage();

    @AfterClass
    public void tearDown() {
        homePage.returnToHomePage();
        homePage.clickOnTheResetIcon();
    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "productsDataProvider")
    public void beetle_user_can_complete_a_order(Product product) {
        Account account = new Account("beetle", "choochoo");
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        modal.typeInUsernameField(account.getUsername());
        modal.typeInPasswordField(account.getPassword());
        modal.clickOnTheLoginButton();
        assertTrue(homePage.validateModalIsNotDisplayed(), "Expected modal to be closed.");
        Header header = new Header(account.getUsername());
        assertEquals(header.getGreetingsMessage(), account.getGreetingsMsg(), "Expected message to be Hi " + account.getUsername() + "!");
        product.addProductToBasket();
        homePage.goToCart();
        CartPage cartPage = new CartPage(product);
        cartPage.clickOnCheckoutButton();
        PersonalInfoPage personalInfoPage = new PersonalInfoPage();
        Person person = new Person("Mihai", "Popescu", "Strada Mare, nr.10, Cluj-Napoca, jud.Cluj");
        personalInfoPage.typeInFirstNameField(person.getFirstName());
        personalInfoPage.typeInLastNameField(person.getLastName());
        personalInfoPage.typeInAddressField(person.getAddress());
        personalInfoPage.clickOnContinueCheckoutButton();
        OrderSummaryPage orderSummaryPage = new OrderSummaryPage();
        orderSummaryPage.clickOnCompleteYourOrderButton();
        OrderCompletePage orderCompletePage = new OrderCompletePage();
        assertTrue(orderCompletePage.validateOrderCompleteMessageIsDisplayed(), "Expected order complete message to be displayed.");
        homePage.returnToHomePage();
        homePage.clickOnTheResetIcon();

    }
}
