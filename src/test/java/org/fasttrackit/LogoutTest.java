package org.fasttrackit;

import org.fasttrackit.body.Header;
import org.fasttrackit.body.Modal;
import org.fasttrackit.dataprovider.AccountDataProvider;
import org.fasttrackit.pages.MainPage;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class LogoutTest {

    MainPage homePage = new MainPage();

    @Test(dataProviderClass = AccountDataProvider.class, dataProvider = "AccountDataProvider")
    public void user_can_logout_from_valid_user(Account account) {
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        modal.typeInUsernameField(account.getUsername());
        modal.typeInPasswordField(account.getPassword());
        modal.clickOnTheLoginButton();
        assertTrue(homePage.validateModalIsNotDisplayed(), "Expected modal to be closed.");
        Header header = new Header();
        sleep(2000);
        homePage.logUserOut();
        assertEquals(header.getGreetingsMessage(), "Hello guest!", "Expected greetings message to be Hello guest!");
        homePage.clickOnTheResetIcon();
    }

}
