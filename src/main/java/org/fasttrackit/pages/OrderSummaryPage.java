package org.fasttrackit.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class OrderSummaryPage {
    private final SelenideElement completeYourOrderButton;
    private final SelenideElement cancelButton;

    public OrderSummaryPage() {
        this.completeYourOrderButton = $(".btn-success[href='#/checkout-complete']");
        this.cancelButton = $(".btn-danger[href='#/cart']");
    }

    public void clickOnCompleteYourOrderButton() {
        this.completeYourOrderButton.click();
    }
    public void clickOnCancelButton() {
        this.cancelButton.click();
    }
}
