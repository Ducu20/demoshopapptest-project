package org.fasttrackit;

import org.fasttrackit.body.Header;
import org.fasttrackit.body.Modal;
import org.fasttrackit.body.QuestionIconModal;
import org.fasttrackit.pages.CartPage;
import org.fasttrackit.pages.MainPage;
import org.fasttrackit.pages.WishlistPage;
import org.fasttrackit.products.Product;
import org.fasttrackit.products.ProductExpectedResults;


public class DemoShopApp {
    private static final String APP_TITLE = "- = Demo Shop Testing simulator = -";

    public static final String DEMO_SHOP_TITLE = "Demo shop";

    public static void main(String[] args) {
        System.out.println(APP_TITLE);
        MainPage homePage = new MainPage();

        verifyStaticPage(homePage);
        verifyLoginModal(homePage);
        verifyWishlistPage(homePage);
        verifyCartPage(homePage);
        verifyQuestionIconModal(homePage);

        String beetleUser = "beetle";
        loginWithUser(homePage, beetleUser);
        validateLoggedInUser(homePage, beetleUser);

    }

    private static void validateLoggedInUser(MainPage homePage, String beetleUser) {
        Header loggedInHeader = new Header(beetleUser);
        homePage.setHeader(loggedInHeader);
        homePage.validateThatHeaderContainsAllElements();
    }

    private static void loginWithUser(MainPage homePage, String beetleUser) {
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        modal.clickOnUsernameField();
        modal.typeInUsernameField(beetleUser);
        modal.clickOnPasswordField();
        modal.typeInPasswordField("choochoo");
        modal.clickOnTheLoginButton();
    }

    private static void verifyWishlistPage(MainPage homePage) {
        homePage.validateWishlistIcon();
        WishlistPage wishlistPage = new WishlistPage(new Product("1", new ProductExpectedResults("", "")));
        wishlistPage.validateThatTheWishlistPageIsDisplayed();
        wishlistPage.clickOnTheLogoIcon();
    }

    private static void verifyLoginModal(MainPage homePage) {
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        modal.validateModalComponents();
        modal.clickOnCloseButton();
        homePage.validateModalIsNotDisplayed();
    }

    private static void verifyQuestionIconModal(MainPage homePage) {
        homePage.clickOnTheQuestionIcon();
        QuestionIconModal questionIconModal = new QuestionIconModal();
        questionIconModal.validateQuestionIconModalComponents();
        questionIconModal.clickOnCloseButton();
        homePage.validateQuestionIconModalIsNotDisplayed();
    }

    private static void verifyStaticPage(MainPage homePage) {
        homePage.getPageTitle();
        homePage.validateThatFooterContainsAllElements();
        homePage.validateThatHeaderContainsAllElements();

    }

    private static void verifyCartPage(MainPage homePage) {
        homePage.validateCartIcon();
        CartPage cartPage = new CartPage(new Product("1", new ProductExpectedResults("", "")));
        cartPage.validateThatTheCartPageIsDisplayed();
        homePage.clickOnTheLogoIcon();
    }
}