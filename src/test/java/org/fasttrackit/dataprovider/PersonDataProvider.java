package org.fasttrackit.dataprovider;

import org.fasttrackit.Person;
import org.testng.annotations.DataProvider;

public class PersonDataProvider {

    @DataProvider(name = "PersonDataProvider")
    public static Object [][] createPersonProvider() {
        Person person = new Person("Mihai", "Popescu", "Strada Mare, nr.10, Cluj-Napoca, jud.Cluj");

        return new Object[][]{
                {person}
        };
    }
}
