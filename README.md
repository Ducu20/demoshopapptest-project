# Final project for Demo Shop application testing

## Short description
This application is responsible for testing the main functionalitys of the Demo Shop app.
 - Login
 - Logout
 - Search
 - Sort drop down menu
 - Pages
 - Products


### Tech used:
- Java 17
- Maven
- Selenide Framework

### Users:
    - guest user*
    - beetle
    - dino
    - locked
    - turtle
> password for all users: choochoo

### Data Providers:
    - Account Data Provider
    - Person Data Provider
    - Product Data Provider

### Pages:
    - Home page
    - Cart page 
    - Wishlist page
    - Personal info page
    - Order summary page
    - Order complete page
>Other page components: header, footer, modal

### How to run the tests
`git clone https://gitlab.com/Ducu20/demoshopapptest-project.git

Execute the following commands to:

#### Execute all tests
- `mvn clean test`

#### Generate Report
- `mvn allure:report`
> Open and present report
- `mvn allure:serve`