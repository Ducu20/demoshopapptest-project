package org.fasttrackit;

import org.fasttrackit.dataprovider.ProductDataProvider;
import org.fasttrackit.pages.CartPage;
import org.fasttrackit.pages.MainPage;
import org.fasttrackit.pages.PersonalInfoPage;
import org.fasttrackit.products.Product;
import org.fasttrackit.products.ProductExpectedResults;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.assertTrue;

public class CartPageTest {

    MainPage homePage = new MainPage();

    @BeforeMethod
    public void reset() {
        homePage.clickOnTheResetIcon();
    }

    @AfterMethod
    public void setup() {
        homePage.returnToHomePage();
    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "productsDataProvider")
    public void user_can_delete_product_from_cart(Product product) {
        product.addProductToBasket();
        homePage.goToCart();
        sleep(3000);
        CartPage cartPage = new CartPage(product);
        cartPage.clickOnTrashIcon();
        sleep(3000);
        assertTrue(cartPage.validateProductIsDeleteFromCart(), "Expected the product to be deleted from cart.");
    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "productsDataProvider")
    public void user_can_increase_number_of_the_product_from_cartPage(Product product) {
        product.addProductToBasket();
        homePage.goToCart();
        sleep(3000);
        CartPage cartPage = new CartPage(product);
        cartPage.clickOnPlusButton();
        sleep(3000);
        assertTrue(cartPage.validateProductIsIncreaseInCart(), "Expected the product to be increase.");

    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "productsDataProvider")
    public void user_can_decrease_number_of_the_product_from_cartPage(Product product) {
        product.addProductToBasket();
        homePage.goToCart();
        sleep(3000);
        CartPage cartPage = new CartPage(product);
        cartPage.clickOnPlusButton();
        sleep(1000);
        cartPage.clickOnMinusButton();
        sleep(3000);
        assertTrue(cartPage.validateProductIsDecreaseInCart(), "Expected the product to be decrease.");

    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "productsDataProvider")
    public void user_can_click_on_continue_shopping_button(Product product) {
        product.addProductToBasket();
        homePage.goToCart();
        sleep(3000);
        CartPage cartPage = new CartPage(product);
        cartPage.clickOnContinueShoppingButton();
        sleep(3000);
        assertTrue(homePage.validateMainPageIsDisplayed(), "Expected the main page to be displayed.");

    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "productsDataProvider")
    public void user_can_click_on_checkout_button(Product product) {
        product.addProductToBasket();
        homePage.goToCart();
        sleep(2000);
        CartPage cartPage = new CartPage(product);
        cartPage.clickOnCheckoutButton();
        sleep(2000);
        PersonalInfoPage personalInfoPage = new PersonalInfoPage();
        assertTrue(personalInfoPage.validatePersonalInfoPageIsDisplayed(), "Expected info page to be displayed.");
        homePage.returnToHomePage();
    }

}
