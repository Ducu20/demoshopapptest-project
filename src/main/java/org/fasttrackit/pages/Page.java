package org.fasttrackit.pages;

import com.codeborne.selenide.Selenide;

import static com.codeborne.selenide.Selenide.open;

public class Page {
    public Page() {
        open("https://fasttrackit-test.netlify.app/");
        System.out.println("Opening Page.");
    }
}
