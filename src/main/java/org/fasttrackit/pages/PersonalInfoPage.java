package org.fasttrackit.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class PersonalInfoPage {

    private final SelenideElement firstNameField;
    private final SelenideElement lastNameField;
    private final SelenideElement addressField;
    private final SelenideElement personalInfoDetails;
    private final SelenideElement creditCardOption;
    private final SelenideElement continueCheckoutButton;
    private final SelenideElement cancelButton;
    private final SelenideElement firstNameErrorMessage;
    private final SelenideElement lastNameErrorMessage;
    private final SelenideElement addressErrorMessage;
    private final SelenideElement orderSummaryPage;
    private final SelenideElement infoPage;


    public PersonalInfoPage() {
        this.firstNameField = $("#first-name");
        this.lastNameField = $("#last-name");
        this.addressField = $("#address");
        this.personalInfoDetails = $(".justify-content-between.row");
        this.creditCardOption = $("div:nth-child(4) div.form-check:nth-child(2) > input.form-check-input");
        this.continueCheckoutButton = $(".btn.btn-success");
        this.cancelButton = $(".btn.btn-danger");
        this.firstNameErrorMessage = $("p.error:nth-child(1)");
        this.lastNameErrorMessage = $("p.error:nth-child(1)");
        this.addressErrorMessage = $("p.error:nth-child(1)");
        this.orderSummaryPage = $("#root");
        this.infoPage = $(".container-fluid");
    }

    public void typeInFirstNameField(String firstNameToType) {
        this.firstNameField.click();
        this.firstNameField.sendKeys(firstNameToType);
    }

    public void typeInLastNameField(String lastNameToType) {
        this.lastNameField.click();
        this.lastNameField.sendKeys(lastNameToType);
    }

    public void typeInAddressField(String addressToType) {
        this.addressField.click();
        this.addressField.sendKeys(addressToType);

    }

    public void selectCreditCardPaymentMethod() {
        this.creditCardOption.click();
    }

    public void clickOnContinueCheckoutButton() {
        this.continueCheckoutButton.click();
    }

    public void clickOnCancelButton() {
        this.cancelButton.click();
    }


    public boolean validatePersonalInfoDetailsAreDisplayed() {
        return this.personalInfoDetails.exists() && this.personalInfoDetails.isDisplayed();
    }

    public boolean validateFirstNameFieldIsDisplayed() {
        return this.firstNameField.exists() && this.firstNameField.isDisplayed();
    }

    public boolean validateLastNameFieldIsDisplayed() {
        return this.lastNameField.exists() && this.lastNameField.isDisplayed();
    }

    public boolean validateAddressFieldIsDisplayed() {
        return this.addressField.exists() && this.addressField.isDisplayed();
    }

    public boolean validateFirstNameErrorMessageIsDisplayed() {
        return this.firstNameErrorMessage.exists() && this.firstNameErrorMessage.isDisplayed();
    }

    public boolean validateLastNameErrorMessageIsDisplayed() {
        return this.lastNameErrorMessage.exists() && this.lastNameErrorMessage.isDisplayed();
    }
    public boolean validateAddressErrorMessageIsDisplayed() {
        return this.addressErrorMessage.exists() && this.addressErrorMessage.isDisplayed();
    }
    public boolean validateOrderSummaryPageIsDisplayed() {
        return this.orderSummaryPage.isDisplayed();
    }
    public boolean validatePersonalInfoPageIsDisplayed()  {
        return this.infoPage.isDisplayed();
    }
    public boolean validateCreditCardOptionIsSelectedAndDisplayed() {
        return this.creditCardOption.isSelected() && this.creditCardOption.isDisplayed();
    }

}
