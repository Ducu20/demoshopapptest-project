package org.fasttrackit.products;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class Product {

    private final String productId;

    private final SelenideElement productLink;

    private final SelenideElement cart;

    private final SelenideElement addToBasketButton;

    private final SelenideElement addToFavorites;

    private final ProductExpectedResults expectedResults;

    public Product(String productId, ProductExpectedResults expectedResults) {
        this.productId = productId;
        this.productLink = $(String.format(".card-body [href='#/product/%s']", productId));
        this.expectedResults = expectedResults;
        this.cart = productLink.parent().parent();
        this.addToBasketButton = cart.$(".fa-cart-plus");
        this.addToFavorites = cart.$(".fa-heart");
    }



    public String getProductId() {
        return productId;
    }
    public String getProductLink() {
        return productLink.text();
    }

    public void clickOnProduct() {
        this.productLink.click();
    }

    public void addProductToBasket() {
        this.addToBasketButton.click();
    }

    public void addToFavorites() {
        this.addToFavorites.click();
    }

    public ProductExpectedResults getExpectedResults() {
        return expectedResults;
    }

    @Override
    public String toString() {
        return this.productLink.text();
    }
}
