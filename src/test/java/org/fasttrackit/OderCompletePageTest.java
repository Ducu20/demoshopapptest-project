package org.fasttrackit;

import org.fasttrackit.dataprovider.ProductDataProvider;
import org.fasttrackit.pages.*;
import org.fasttrackit.products.Product;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class OderCompletePageTest {

    MainPage homePage = new MainPage();

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "productsDataProvider")
    public void user_can_click_on_continue_shopping_button(Product product) {
        product.addProductToBasket();
        homePage.goToCart();
        CartPage cartPage = new CartPage(product);
        cartPage.clickOnCheckoutButton();
        PersonalInfoPage personalInfoPage = new PersonalInfoPage();
        Person person = new Person("Mihai", "Popescu", "Strada Mare, nr.10, Cluj-Napoca, jud.Cluj");
        personalInfoPage.typeInFirstNameField(person.getFirstName());
        personalInfoPage.typeInLastNameField(person.getLastName());
        personalInfoPage.typeInAddressField(person.getAddress());
        personalInfoPage.clickOnContinueCheckoutButton();
        OrderSummaryPage orderSummaryPage = new OrderSummaryPage();
        orderSummaryPage.clickOnCompleteYourOrderButton();
        sleep(2000);
        OrderCompletePage orderCompletePage = new OrderCompletePage();
        orderCompletePage.clickOnContinueShoppingButton();
        sleep(2000);
        assertTrue(homePage.validateMainPageIsDisplayed(), "Expected main page to be displayed.");
        homePage.returnToHomePage();
    }
}
