package org.fasttrackit;

import org.fasttrackit.dataprovider.ProductDataProvider;
import org.fasttrackit.pages.MainPage;
import org.fasttrackit.pages.WishlistPage;
import org.fasttrackit.products.Product;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class WishlistPageTest {
    MainPage homePage = new MainPage();

    @BeforeMethod
    public void reset() {
        homePage.clickOnTheResetIcon();
    }

    @AfterMethod
    public void setup() {
        homePage.returnToHomePage();
    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "productsDataProvider")
    public void user_can_add_products_to_wishlist(Product product) {
        product.addToFavorites();
        homePage.goToWishlist();
        WishlistPage wishlistPage = new WishlistPage(product);
        sleep(2000);
        assertTrue(wishlistPage.validateWishlistPageIsDisplayed(), "Expected product to be displayed in wishlist page.");
        homePage.returnToHomePage();
        homePage.clickOnTheResetIcon();

    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "productsDataProvider")
    public void user_can_delete_product_from_wishlist(Product product) {
        product.addToFavorites();
        homePage.goToWishlist();
        WishlistPage wishlistPage = new WishlistPage(product);
        sleep(2000);
        wishlistPage.clickOnBrokenHeartButton();
        sleep(2000);
        assertTrue(wishlistPage.validateProductIsNotDisplayed(), "Expected product to not exist in wishlist page.");
        homePage.returnToHomePage();
        homePage.clickOnTheResetIcon();
    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "productsDataProvider")
    public void user_can_add_product_to_cart_from_wishlist(Product product) {
        product.addToFavorites();
        homePage.goToWishlist();
        WishlistPage wishlistPage = new WishlistPage(product);
        sleep(2000);
        wishlistPage.clickOnCartButton();
        sleep(2000);
        assertTrue(wishlistPage.validateProductIsDisplayedInCart(), "Expected that the product to be displayed in cart.");
        homePage.returnToHomePage();
        homePage.clickOnTheResetIcon();
    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "productsDataProvider")
    public void user_can_increase_number_of_product_from_wishlist_page(Product product) {
        product.addToFavorites();
        homePage.goToWishlist();
        WishlistPage wishlistPage = new WishlistPage(product);
        sleep(2000);
        wishlistPage.clickOnCartButton();
        sleep(1000);
        wishlistPage.clickOnCartButton();
        sleep(1000);
        wishlistPage.clickOnCartButton();
        sleep(2000);
        assertTrue(wishlistPage.validateProductIsDisplayedInCart(), "Expected the products to increase in cart.");
        homePage.returnToHomePage();
        homePage.clickOnTheResetIcon();
    }

}
