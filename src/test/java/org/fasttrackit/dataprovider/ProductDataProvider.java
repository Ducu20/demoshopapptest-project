package org.fasttrackit.dataprovider;

import org.fasttrackit.products.Product;
import org.fasttrackit.products.ProductExpectedResults;
import org.testng.annotations.DataProvider;

public class ProductDataProvider {

    @DataProvider(name = "productsDataProvider")
    public static Object[][] createProductsProvider() {
        Product product1 = new Product("1", new ProductExpectedResults("Awesome Granite Chips", "15.99 EUR"));
        Product product2 = new Product("2", new ProductExpectedResults("Incredible Concrete Hat", "7.99 EUR"));
        Product product3 = new Product("3", new ProductExpectedResults("Awesome Metal Chair", "15.99 EUR"));
        Product product4 = new Product("4", new ProductExpectedResults("Practical Wooden Bacon", "29.99 EUR"));
        Product product5 = new Product("5", new ProductExpectedResults("Awesome Soft Shirt", "29.99 EUR"));
        Product product6 = new Product("6", new ProductExpectedResults("Practical Wooden Bacon", "1.99 EUR"));
        Product product7 = new Product("7", new ProductExpectedResults("Practical Metal Mouse", "9.99 EUR"));
        Product product8 = new Product("8", new ProductExpectedResults("Licensed Steel Gloves", "14.99 EUR"));
        Product product9 = new Product("9", new ProductExpectedResults("Gorgeous Soft Pizza", "19.99 EUR"));
        Product product10 = new Product("0", new ProductExpectedResults("Refined Frozen Mouse", "9.99 EUR"));


        return new Object[][]{
                {product1},
                {product2},
                {product3},
                {product4},
                {product5},
                {product6},
                {product7},
                {product8},
                {product9},
                {product10}

        };
    }

}
