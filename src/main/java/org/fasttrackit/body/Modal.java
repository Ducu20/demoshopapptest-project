package org.fasttrackit.body;

import com.codeborne.selenide.ClickOptions;
import com.codeborne.selenide.SelenideElement;

import java.time.Duration;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;

public class Modal {
    private final SelenideElement modalTitle = $(".modal-title");
    private final SelenideElement closeButton = $(".close");

    private final SelenideElement username = $("#user-name");
    private final SelenideElement password = $("#password");
    private final SelenideElement loginButton = $(".btn.btn-primary");

    private final SelenideElement attentionMessage = $(".error");


    public Modal() {
    }

    public String getModalTitle() {
        return modalTitle.text();
    }


    public void validateModalComponents() {
    }


    /**
     * Clicks
     */
    public void clickOnCloseButton() {
        System.out.println("Clicked on the 'x' button.");
        this.closeButton.click();
        sleep(100);
    }

    public void clickOnUsernameField() {
        this.username.click();
    }

    public void clickOnPasswordField() {

        System.out.println("Clicked on the " + this.password);
    }

    public void clickOnTheLoginButton() {
        this.loginButton.click();
        sleep(150);
    }

    /**
     * Types
     */

    public void typeInUsernameField(String userToType) {
        System.out.println("Typed in username field: " + userToType);
        this.username.click();
        this.username.sendKeys(userToType);
    }


    public void typeInPasswordField(String passwordToType) {
        System.out.println("Typed in password field: " + passwordToType);
        this.password.click();
        this.password.sendKeys(passwordToType);
    }

    /**
     * Validators
     */

    public boolean validateCloseButtonIsDisplayed() {
        return this.closeButton.exists() && this.closeButton.isDisplayed();
    }

    public boolean validateUserNameFieldIsDisplayed() {

        return this.username.exists() && this.username.isDisplayed();
    }

    public boolean validatePasswordFieldIsDisplayed() {

        return this.password.exists() && this.password.isDisplayed();
    }

    public boolean validateLoginButtonIsDisplayedAndEnable() {
        return this.loginButton.exists() && this.loginButton.isDisplayed() && this.loginButton.isEnabled();
    }

    public boolean validateAttentionMessageIsDisplayed() {
        return this.attentionMessage.exists();

    }


}
