package org.fasttrackit.body;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class Header {
    private final SelenideElement logoIconUrl = $(".fa-shopping-bag");
    private final SelenideElement shoppingCartIconUrl = $(".fa-shopping-cart");
    private final SelenideElement wishlistIconUrl = $(".fa-heart");
    private final String greetingsMessage;


    private final SelenideElement signInButton = $(".fa-sign-in-alt");
    private final SelenideElement signOutButton = $(".fa-sign-out-alt");


    public Header() {

        this.greetingsMessage = "Hello guest!";


    }

    public Header(String user) {

        this.greetingsMessage = "Hi " + user + "!";


    }

    /**
     * Getters
     */

    public SelenideElement getLogoIconUrl() {
        return logoIconUrl;
    }

    public SelenideElement getShoppingCartIconUrl() {
        return shoppingCartIconUrl;
    }

    public SelenideElement getWishlistIconUrl() {
        return wishlistIconUrl;
    }

    public String getGreetingsMessage() {
        return greetingsMessage;
    }

    public SelenideElement getSignInButton() {
        return signInButton;
    }

    /**
     * Actions
     */

    public void clickOnTheLoginButton() {
        System.out.println("Clicked on the Login button.");
        this.signInButton.click();
    }

    public void clickOnTheLogoutButton() {
        this.signOutButton.click();
    }

    public void clickOnTheWishlistIcon() {
        System.out.println("Clicked on the Wishlist icon.");
    }

    public void clickOnTheLogoIcon() {
        System.out.println("Return to the homepage.");
    }

    public void clickOnTheCartIcon() {
        System.out.println("Clicked on the Cart icon.");
    }
}
