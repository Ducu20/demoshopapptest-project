package org.fasttrackit.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class OrderCompletePage {

    private final SelenideElement orderCompleteMessage;
    private final SelenideElement continueShoppingButton;

    public OrderCompletePage() {
        this.orderCompleteMessage = $(".text-center");
        this.continueShoppingButton = $(".btn-success[href='#/products']");
    }

    public boolean validateOrderCompleteMessageIsDisplayed() {
        return this.orderCompleteMessage.exists() && this.orderCompleteMessage.isDisplayed();
    }
    public void clickOnContinueShoppingButton() {
        this.continueShoppingButton.click();
    }


}

