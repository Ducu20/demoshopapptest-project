package org.fasttrackit.body;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class QuestionIconModal {


    private final SelenideElement questionIconModalTitle = $(".modal-title.h4");
    private final SelenideElement closeButton = $(".close");



    public String getQuestionIconModalTitle() {
        return this.questionIconModalTitle.text();

    }

    public void validateQuestionIconModalComponents() {
    }

    public void clickOnCloseButton() {
        this.closeButton.click();
    }

}

